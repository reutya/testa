<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Customer;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\DB;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    //     $id = Auth::id();
    //     $user = User::find($id);
    //    // var_dump($user);
    //     //die();
    //     $customers = $user->customers;
    //    // var_dump($customers);
    //     //die();
    //     return view('customers.index', ['customers'=>$customers]);
        $customers = Customer::all();
        // $ch = DB::table('users')->join(DB::table('customers'),$operator= $customers->user_id);
        return view('customers.index', compact('customers'));

    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('customers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $customer= new Customer();
    
        $id = Auth::id();
       
        $customer->name = $request->name;
        $customer->email = $request->email;
        $customer->phone= $request->phone;
        $customer->user_id = $id;
        $customer->status=0;
        $customer->save();
        return redirect('customers');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customer=Customer::find($id);
            
            
        if(Gate::allows('user')){
            if($customer->user->id != Auth::id()) 
            abort(403," you are a user. this is not your customer you are not allowed to edit"); 
            return view('customers.edit',compact('customer'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // $customer = Customer::find($id);
        // $customer->update($request->all());
        // return redirect('customers');  
        
        $customer = Customer::find($id);
       
        if(Gate::allows('user')){
            if($customer->user->id != Auth::id()) 
            abort(403," you are a user this is not your customer you are not allowed to edit"); 
        else
        
        $customer->update($request->all());
        return redirect('customers');  
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('manager')) {
            abort(403,"users are not allowed to delete");
       }
        $customer = Customer::find($id);
        $customer->delete();
        return redirect('customers');;
    }

    public function done($id)
    {
        if (Gate::denies('manager')) {
            abort(403,"users are not allowed to mark closed deals");
       }
        $customer = Customer::find($id);
        $customer->status = 1; 
        $customer->save();
        return redirect('customers');    

    }
}
