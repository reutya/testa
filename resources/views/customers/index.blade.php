@extends('layouts.app')
@section('content')

<!DOCTYPE html>
<html>
<head><h3>This is your customers List</h3></head>

<body>
<a href = "{{route('customers.create')}}" >add new customer</a>

<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
</style>

<table class="table table-striped table-bordered table-responsive table-hover table-condensed alpha">
    <tr>
     <th>user who takes care of this customer</th>
     <th>name</th>
     <th>email</th>
     <th>phone</th>
     <th>edit</th>
     <th>delete</th>

     <th>deal status</th>
    </tr>

    @foreach($customers as $customer)
    @if ($customer->status == 0)
    <tr>
    
        <td >{{$customer->user_id}}</td>
        <td>{{$customer->name}}</td>
        <td>{{$customer->email}}</td>
        <td>{{$customer->phone}}</td>
        <td><a href = "{{route('customers.edit',$customer->id)}}" >edit </a></td>
        
        <td>
        @can('manager')
        <a href = "{{route('delete',$customer->id)}}">  delete </a>
        @csrf
        @method('DELETE')
        @endcan
        
        @can('user')
        delete
        @endcan
        </td>
        <td>
        @if ($customer->status == 0)
            @can('manager')
            <a href="{{route('done', $customer->id)}}" >deal closed</a>
            @endcan     
        @else
            deal is done
        
        @endif

        </td>
   </tr>

 @else
 <tr style="color:green">
    
    <td >{{$customer->user_id}}</td>
    <td>{{$customer->name}}</td>
    <td>{{$customer->email}}</td>
    <td>{{$customer->phone}}</td>
    <td><a href = "{{route('customers.edit',$customer->id)}}" >edit {{$customer->name}}</a></td>
    
    <td>
    @can('manager')
    <a href = "{{route('delete',$customer->id)}}">  delete {{$customer->name}} </a>
    @csrf
    @method('DELETE')
    @endcan
    
    @can('user')
    delete
    @endcan
    </td>
    <td>
    @if ($customer->status == 0)
        @can('manager')
        <a href="{{route('done', $customer->id)}}" >deal closed</a>
        @endcan     
    @else
        deal is done
    
    @endif

    </td>
</tr>

 @endif



   @endforeach

</table>








</body>
</html>
@endsection