

@extends('layouts.app')
@section('content')


<h1>create new customer</h1>
<form method  = 'post' action = "{{action('CustomerController@store')}}">
{{csrf_field()}}
<div>
    <label for = "name">Customer name: </label>
    <input type ="text" class = "form-control" name = "name">
</div>

<div>
    <label for = "email">add the email: </label>
    <input type ="text" class = "form-control" name = "email">
</div>

<div>
    <label for = "phone">add the phone: </label>
    <input type ="text" class = "form-control" name = "phone">
</div>

<ul>
@foreach($errors->all() as $error)
   <li>{{$error}}</li>
@endforeach
</ul>

<div class = "form-group">
    <input type ="submit" class = "form-control" name = "submit" value="save">
</div>

</form>
@endsection